package web.functions;

import java.net.URISyntaxException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import web.Driver;

import com.applitools.eyes.Eyes;
import com.applitools.eyes.MatchLevel;
import com.applitools.eyes.NewTestException;
import com.applitools.eyes.RectangleSize;
import common.Execution;

public class Camera {
	
	public static void captureWindow(String appName, String testName, String windowName, String matchLevel)
			throws URISyntaxException, InterruptedException, NewTestException {
			Eyes eyes = new Eyes();
		try {
			
			eyes.setApiKey(Execution.apiKey);
			switch (matchLevel) {
			case "hide":
				eyes.setMatchLevel(MatchLevel.NONE);
				break;
			case "strict":
				eyes.setMatchLevel(MatchLevel.STRICT);
				break;
			case "content":
				eyes.setMatchLevel(MatchLevel.CONTENT);
				break;
			case "layout":
				eyes.setMatchLevel(MatchLevel.LAYOUT);
				break;
			default:
				eyes.setMatchLevel(MatchLevel.EXACT);
				break;
			}

			// [Henry_160115]
			int xSolution = Integer.parseInt(Execution.screenSolution.split(",")[0]);
			int ySolution = Integer.parseInt(Execution.screenSolution.split(",")[1]);
			eyes.open(Driver.instance, appName, testName, new RectangleSize(
					xSolution, ySolution));
			eyes.checkWindow(windowName);

			eyes.close();
		} finally {
			eyes.abortIfNotClosed();
		}
	}
	
	public static void captureWindow(String appName, String testName, String windowName, String matchLevel, int xResolution, int yResolution)
			throws URISyntaxException, InterruptedException, NewTestException {
			Eyes eyes = new Eyes();
		try {
			
			eyes.setApiKey(Execution.apiKey);
			switch (matchLevel) {
			case "hide":
				eyes.setMatchLevel(MatchLevel.NONE);
				break;
			case "strict":
				eyes.setMatchLevel(MatchLevel.STRICT);
				break;
			case "content":
				eyes.setMatchLevel(MatchLevel.CONTENT);
				break;
			case "layout":
				eyes.setMatchLevel(MatchLevel.LAYOUT);
				break;
			default:
				eyes.setMatchLevel(MatchLevel.EXACT);
				break;
			}

			// [Henry_160115]
			eyes.open(Driver.instance, appName, testName, new RectangleSize(
					xResolution, yResolution));
			eyes.checkWindow(windowName);

			eyes.close();
		} finally {
			eyes.abortIfNotClosed();
		}
	}
	
	public static void captureElement(String appName, String testName, WebElement element, String matchLevel)
			throws URISyntaxException, InterruptedException, NewTestException {
		Eyes eyes = new Eyes();
		try {
			
			eyes.setApiKey(Execution.apiKey);
			switch (matchLevel) {
			case "hide":
				eyes.setMatchLevel(MatchLevel.NONE);
				break;
			case "strict":
				eyes.setMatchLevel(MatchLevel.STRICT);
				break;
			case "content":
				eyes.setMatchLevel(MatchLevel.CONTENT);
				break;
			case "layout":
				eyes.setMatchLevel(MatchLevel.LAYOUT);
				break;
			default:
				eyes.setMatchLevel(MatchLevel.EXACT);
				break;
			}
			int xResolution = Integer.parseInt(Execution.screenSolution.split(",")[0]);
			int yResolution = Integer.parseInt(Execution.screenSolution.split(",")[1]);
			eyes.open(Driver.instance, appName, testName, new RectangleSize(
					xResolution, yResolution));
			eyes.checkRegion(element);

			eyes.close();
		} finally {
			eyes.abortIfNotClosed();
		}
	}
	
	public static void captureElement(String appName, String testName, WebElement element, String matchLevel, int xResolution, int yResolution)
			throws URISyntaxException, InterruptedException, NewTestException {
		Eyes eyes = new Eyes();
		try {
			
			eyes.setApiKey(Execution.apiKey);
			switch (matchLevel) {
			case "hide":
				eyes.setMatchLevel(MatchLevel.NONE);
				break;
			case "strict":
				eyes.setMatchLevel(MatchLevel.STRICT);
				break;
			case "content":
				eyes.setMatchLevel(MatchLevel.CONTENT);
				break;
			case "layout":
				eyes.setMatchLevel(MatchLevel.LAYOUT);
				break;
			default:
				eyes.setMatchLevel(MatchLevel.EXACT);
				break;
			}
			eyes.open(Driver.instance, appName, testName, new RectangleSize(
					xResolution, yResolution));
			eyes.checkRegion(element);

			eyes.close();
		} finally {
			eyes.abortIfNotClosed();
		}
	}

	public static void captureElement(String appName, String testName, By seletor)
			throws URISyntaxException, InterruptedException, NewTestException {
		Eyes eyes = new Eyes();
		try {
			eyes = new Eyes();
			eyes.setApiKey(Execution.apiKey);
			eyes.setMatchLevel(MatchLevel.EXACT); // [Henry_160115]
			int xSolution = Integer.parseInt(Execution.screenSolution.split(",")[0]);
			int ySolution = Integer.parseInt(Execution.screenSolution.split(",")[1]);
			eyes.open(Driver.instance, appName, testName, new RectangleSize(
					xSolution, ySolution));
			eyes.checkRegion(seletor);

			eyes.close();
		} finally {
			eyes.abortIfNotClosed();
		}
	}
	
	public void captureElement(String appName, String testName, By seletor, int xResolution, int yResolution)
			throws URISyntaxException, InterruptedException, NewTestException {
		Eyes eyes = new Eyes();
		try {
			eyes = new Eyes();
			eyes.setApiKey(Execution.apiKey);
			eyes.setMatchLevel(MatchLevel.EXACT); // [Henry_160115]
			eyes.open(Driver.instance, appName, testName, new RectangleSize(
					xResolution, yResolution));
			eyes.checkRegion(seletor);

			eyes.close();
		} finally {
			eyes.abortIfNotClosed();
		}
	}

	public void captureFrame(String appName, String testName, WebElement frameReference)
			throws URISyntaxException, InterruptedException, NewTestException {
		Eyes eyes = new Eyes();
		try {
			eyes.setApiKey(Execution.apiKey);
			eyes.setMatchLevel(MatchLevel.EXACT); // [Henry_160115]
			int xResolution = Integer.parseInt(Execution.screenSolution.split(",")[0]);
			int yResolution = Integer.parseInt(Execution.screenSolution.split(",")[1]);
			eyes.open(Driver.instance, appName, testName, new RectangleSize(
					xResolution, yResolution));
			eyes.checkFrame(frameReference);

			eyes.close();
		} finally {
			eyes.abortIfNotClosed();
		}
	}
	public void captureFrame(String appName, String testName, WebElement frameReference, int xResolution, int yResolution)
			throws URISyntaxException, InterruptedException, NewTestException {
		Eyes eyes = new Eyes();
		try {
			eyes.setApiKey(Execution.apiKey);
			eyes.setMatchLevel(MatchLevel.EXACT); // [Henry_160115]
			eyes.open(Driver.instance, appName, testName, new RectangleSize(
					xResolution, yResolution));
			eyes.checkFrame(frameReference);

			eyes.close();
		} finally {
			eyes.abortIfNotClosed();
		}
	}
}
