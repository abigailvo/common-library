package web.functions;

import java.awt.Color;
import java.io.File;
import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;

import javax.mail.Folder;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.lift.TestContext;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import web.Driver;

import com.applitools.eyes.NewTestException;
import com.applitools.eyes.TestFailedException;

import common.Execution;
import common.thirdParty.TestRail;

public class VerifyMethods {
	/**
	 * Check the value of an element
	 * 
	 * @param element
	 * @param expectedElementValue
	 */
	public static void verifyElementValue(WebElement element, String expectedElementValue) {
		if (expectedElementValue != null) {
			GeneralMethods.waitForPageFullyLoaded(Execution.pageWait);
			String elementValue = element.getAttribute("value").toString();
			if (elementValue != expectedElementValue) {
				Execution.setTestFail("The value '" + elementValue + "' of '" + element.toString()
						+ "' is different from the expected '" + expectedElementValue + "'!");
			}
		}
	}

	/**
	 * Check the value of an element
	 * 
	 * @param element
	 * @param elementName
	 * @param expectedElementValue
	 */
	public static void verifyElementValue(WebElement element, String elementName, String expectedElementValue) {
		if (expectedElementValue != null) {
			// Driver.instance.SwitchTo().DefaultContent();
			GeneralMethods.waitForPageFullyLoaded(Execution.pageWait);
			String elementValue = element.getAttribute("value").toString();
			// String elementValue = element.Text.ToString().Trim();
			if (elementValue != expectedElementValue) {
				Execution.setTestFail("The value " + elementValue + " of '" + elementName
						+ "' is different from the expected '" + expectedElementValue + "'!");
			}
		}
	}

	/**
	 * Check the text of an element. The breakline will be removed
	 * 
	 * @param element
	 * @param elementName
	 * @param expectedElementText
	 */
	public static void verifyElementText(WebElement element, String elementName, String expectedElementText) {
		if (expectedElementText != null) {
			// Driver.instance.SwitchTo().DefaultContent();
			GeneralMethods.waitForPageFullyLoaded(Execution.pageWait);
			// String elementValue = element.GetAttribute("value").ToString();
			String elementText = element.getText().toString().trim().replace("\r\n", "");
			if (elementText != expectedElementText) {
				Execution.setTestFail("The text of '" + elementName + "' is different from the expected '"
						+ expectedElementText + "'!");
			}
		}
	}

	/**
	 * Check whether a text of an element contains a sub-string
	 * 
	 * @param element
	 * @param elementName
	 * @param expectedContainedText
	 */
	public static void verifyElementTextContains(WebElement element, String elementName, String expectedContainedText) {
		if (expectedContainedText != null) {
			GeneralMethods.waitForPageFullyLoaded(Execution.pageWait);
			// String elementValue = element.GetAttribute("value").ToString();
			String elementText = element.getText().toString().trim().replace("\r\n", "");
			if (!elementText.contains(expectedContainedText)) {
				Execution.setTestFail("The text of '" + elementName + "' does not contain the expected text'"
						+ expectedContainedText + "'!");
			}
		}
	}

	/**
	 * Check if an element exists
	 * 
	 * @param elementBy
	 */
	public static void verifyElementExists(By elementBy) {
		// WaitForPageLoad(Selenium.pageWait);
		if (!GeneralMethods.doesElementExist(elementBy)) {
			Execution.setTestFail("The element '" + elementBy.toString() + "' does not exist!");
		}
	}

	/**
	 * Check if an element exists
	 * 
	 * @param elementBy
	 * @param elementName
	 */
	public static void verifyElementExists(By elementBy, String elementName) {
		// WaitForPageLoad(Selenium.pageWait);
		if (!GeneralMethods.doesElementExist(elementBy)) {
			Execution.setTestFail("The element '" + elementName + "' does not exist!");
		}
	}

	public static void verifyElementNotExists(By elementBy) {
		GeneralMethods.waitForPageFullyLoaded(Execution.pageWait);
		if (GeneralMethods.doesElementExist(elementBy)) {
			Execution.setTestFail("The element '" + elementBy.toString() + "' exists!");
		}
	}

	/**
	 * Check if an element does not exist
	 * 
	 * @param elementBy
	 * @param elementName
	 */
	public static void verifyElementNotExists(By elementBy, String elementName) {
		GeneralMethods.waitForPageFullyLoaded(Execution.pageWait);
		if (GeneralMethods.doesElementExist(elementBy)) {
			Execution.setTestFail("The element '" + elementName + "' exists!");
		}
	}

	/**
	 * Check if an element is enabled
	 * 
	 * @param element
	 */
	public static void verifyElementEnabled(WebElement element) {
		GeneralMethods.waitForPageFullyLoaded(Execution.pageWait);
		if (!element.isEnabled()) {
			Execution.setTestFail("The element '" + element.toString() + "' is not enabled!");
		}

	}

	/**
	 * Check if an element is enabled
	 * 
	 * @param element
	 * @param elementName
	 */
	public static void verifyElementEnabled(WebElement element, String elementName) {
		GeneralMethods.waitForPageFullyLoaded(Execution.pageWait);
		if (!element.isEnabled()) {
			Execution.setTestFail("The element '" + elementName + "' is not enabled!");
		}
	}

	/**
	 * Check if an element is disabled
	 * 
	 * @param element
	 */
	public static void verifyElementDisabled(WebElement element) {
		GeneralMethods.waitForPageFullyLoaded(Execution.pageWait);
		if (element.isEnabled()) {
			Execution.setTestFail("The element '" + element.toString() + "' is enabled!");
		}

	}

	/**
	 * Check if an element is disabled
	 * 
	 * @param element
	 * @param elementName
	 */
	public static void verifyElementDisabled(WebElement element, String elementName) {
		GeneralMethods.waitForPageFullyLoaded(Execution.pageWait);
		if (element.isEnabled()) {
			Execution.setTestFail("The element '" + elementName + "' is enabled!");
		}

	}

	/**
	 * Check the value of the selected item of a DropDown list
	 * 
	 * @param element
	 * @param elementName
	 * @param expectedValue
	 */
	public static void verifySelectedItem(WebElement element, String elementName, String expectedValue) {
		if (expectedValue != null) {
			GeneralMethods.waitForPageFullyLoaded(Execution.pageWait);
			org.openqa.selenium.support.ui.Select selectElm = new org.openqa.selenium.support.ui.Select(element);
			if (selectElm.getFirstSelectedOption().toString() != expectedValue) {
				Execution.setTestFail("The value '" + selectElm.getFirstSelectedOption().getText()
						+ "' of the selected item of the element '" + elementName + "' is different from the expected "
						+ expectedValue + "!");
			}
		}
	}

	public static void verifyFileExists(String filePath) {
		File f = new File(filePath);
		if (!f.exists() && f.isDirectory()) {
			Execution.setTestFail(filePath + " does not exists.");
		}
	}

	/**
	 * This method helps upload test result to TestRail
	 */
	public static void verifyTest() {
		addTestResultToTestRail();

		if (!Execution.isPassed) {
			Assert.assertTrue(false);
		} else {
			Assert.assertTrue(true);
		}
	}

	private static void addTestResultToTestRail() {
		// ------- Add test result to TestRail--------------------------
		Method method = Execution.method;
		String configuration = Execution.configuration;
		String className = Execution.className;
		if (Execution.length == 0) {
			Test tstAnnotation = method.getAnnotation(Test.class);
			Class<?> testClass = method.getDeclaringClass();
			for (Method me : testClass.getDeclaredMethods()) {
				if (me.getAnnotation(DataProvider.class) != null) {
					if (tstAnnotation.dataProvider().equals(me.getAnnotation(DataProvider.class).name())) {
						Object[][] ob = null;
						try {
							ob = (Object[][]) me.invoke(null, new Object[] { method });
							Execution.length = ob.length;
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
		if (Execution.count == 0) {
			Execution.arrayTestResult = new ArrayList<Boolean>();
		}

		TestRail tr = new TestRail(method, configuration, className);
		// tr.clientId = Execution.clientId;
		// tr.username = Execution.testrailUsername;
		// tr.password = Execution.testrailPassword;
		// tr.arrKey = Execution.keys;
		// tr.arrComponent = Execution.components;
		// tr.projectId = Execution.projectId;
		// tr.planName = Execution.planName;

		if (!Execution.isPassed) {
			Execution.arrayTestResult.add(false);
			if (Execution.result.equals("")) {
				Execution.result = "" + Execution.count;
			} else
				Execution.result = Execution.result + ", " + (Execution.count + 1);
		} else {
			Execution.arrayTestResult.add(true);
		}

		Execution.count = Execution.count + 1;
		// if (Execution.count >= Execution.length) {
		// if (Execution.arrayTestResult.contains(false)) {
		// tr.statusId = 5;
		// tr.comment = "This test has been run automatically. Failed at
		// variation: " + Execution.result;
		// } else {
		// tr.comment = "This test has been run automatically";
		// tr.statusId = 1;
		// }
		// tr.addResult();
		// Execution.count = 0;
		// Execution.length = 0;
		// }
		// -------------------------------------------------------------------------------------------------
	}

	public static void verifyTextContains(String text, String expected) {
		if (!text.contains(expected)) {
			Execution.setTestFail(text + " does not contain " + expected);
		}
	}

	public static void verifyTextEquals(String actual, String expected) {
		if (actual == null && expected != null) {
			Execution.setTestFail(" Expected: " + expected + " || " + "Actual: null");
			return;
		}

		if (actual != null && expected == null) {
			Execution.setTestFail(" Expected: null || " + "Actual: " + actual);
			return;
		}

		if (actual == null && expected == null) {
			Execution.setTestFail(" Expected: null || Actual: null");
			return;
		}

		if (!actual.equals(expected)) {
			Execution.setTestFail(" Expected: " + expected + " || " + "Actual: " + actual);
		}
	}

	public static void verifyTextEquals(String actual, String expected, String message) {
		if (actual == null && expected != null) {
			Execution.setTestFail(message + " Expected: " + expected + " || " + "Actual: null");
			return;
		}

		if (actual != null && expected == null) {
			Execution.setTestFail(message + " Expected: null || " + "Actual: " + actual);
			return;
		}

		if (actual == null && expected == null) {
			Execution.setTestFail(message + " Expected: null || Actual: null");
			return;
		}

		if (!actual.equals(expected)) {
			Execution.setTestFail(message + " Expected: " + expected + " || " + "Actual: " + actual);
		}
	}

	public static void verifyTextNotContain(String text, String expected) {
		if (text.contains(expected)) {
			Execution.setTestFail("Unexpected: " + text + " contains " + expected);
		}
	}

	public static void verifyTextNotEqual(String actual, String expected) {
		if (actual.equals(expected)) {
			Execution.setTestFail("Unexpected: " + expected + " || " + "Actual: " + actual);
		}
	}

	public static void verifyEquals(Boolean actual, Boolean expected) {
		if (!actual.equals(expected)) {
			Execution.setTestFail("expected: " + expected + " || " + "Actual: " + actual);
		}
	}

	public static void verifyEquals(Boolean actual, Boolean expected, String message) {
		if (!actual.equals(expected)) {
			Execution.setTestFail(message + " expected: " + expected + " || " + "Actual: " + actual);
		}
	}

	public static void verifyEquals(Object actual, Object expected) {
		if (!actual.equals(expected)) {
			Execution.setTestFail("Expected: " + expected + " || " + "Actual: " + actual);
		}
	}

	public static void verifyEquals(Object actual, Object expected, String message) {
		if (!actual.equals(expected)) {
			Execution.setTestFail(message + " Expected: " + expected + " || " + "Actual: " + actual);
		}
	}

	public static void verifyEquals(int actual, int expected) {
		if (actual != expected) {
			Execution.setTestFail("Expected: " + expected + " || " + "Actual: " + actual);
		}
	}

	public static void verifyEquals(List<?> actual, List<?> expected) {
		if (actual != expected) {
			Execution.setTestFail("Expected: " + expected + " || " + "Actual: " + actual);
		}
	}

	public static void verifyEquals(List<?> actual, List<?> expected, String message) {
		if (actual != expected) {
			Execution.setTestFail(message + " Expected: " + expected + " || " + "Actual: " + actual);
		}
	}

	public static void verifyEquals(int actual, int expected, String message) {
		if (actual != expected) {
			Execution.setTestFail(message + " expected: " + expected + " || " + "Actual: " + actual);
		}
	}

	public static void verifyPageTitle(String expected) {
		String actual = Driver.instance.getTitle();
		if (!actual.equals(expected)) {
			Execution.setTestFail("Unexpected: " + expected + " || " + "Actual: " + actual);
		}
	}

	public static void verifyPageTitleContains(String expected) {
		String actual = Driver.instance.getTitle();
		if (!actual.contains(expected)) {
			Execution.setTestFail("Unexpected: " + expected + " || " + "Actual: " + actual);
		}
	}

	/*
	 * public static void verifyWindow(String appName, String testName, String
	 * windowName) { try { //Execution.camera.apiKey = Execution.apiKey;
	 * //Execution.camera.screenSolution = Execution.screenSolution;
	 * //Execution.camera.matchLevel = Execution.matchLevel;
	 * Execution.camera.window(appName, "Window: " + windowName, windowName); }
	 * catch (URISyntaxException e) { e.printStackTrace(); } catch
	 * (InterruptedException e1) { e1.printStackTrace(); } catch
	 * (NewTestException b) { String message = b.getMessage().toString();
	 * message = message.substring(message.indexOf("https")); message =
	 * "Please accept new baseline in " + message; //
	 * JOptionPane.showMessageDialog(null, message, "New Baseline", 1); } catch
	 * (TestFailedException f) { String message = f.getMessage().toString();
	 * message = message.substring(message.indexOf("See")); message =
	 * "verify window " + windowName + ": Failed!\n" + message; //
	 * JOptionPane.showMessageDialog(null, message, "Check Window", 1);
	 * Execution.setTestFail(message); } }
	 */

	public static void verifyWindow(String appName, String testName, String windowName, String matchLevel) {
		try {

			Camera.captureWindow(appName, "Window: " + windowName, windowName, matchLevel);

		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		} catch (NewTestException b) {
			String message = b.getMessage().toString();
			message = message.substring(message.indexOf("https"));
			message = "Please accept new baseline in " + message;
			// JOptionPane.showMessageDialog(null, message, "New Baseline", 1);
		} catch (TestFailedException f) {
			String message = f.getMessage().toString();
			message = message.substring(message.indexOf("See"));
			message = "verify window " + windowName + ": Failed!\n" + message;
			// JOptionPane.showMessageDialog(null, message, "Check Window", 1);
			Execution.setTestFail(message);
		}
	}

	public static void verifyRegion(String appName, String testName, WebElement element) {
		try {
			// Execution.camera.apiKey = apiKey;
			// Execution.camera.screenSolution = screenSolution;
			// Execution.camera.matchLevel = matchLevel;
			Camera.captureElement(appName, "Element: " + testName, element, "exact");
		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		} catch (NewTestException b) {
			String message = b.getMessage().toString();
			message = message.substring(message.indexOf("https"));
			message = "Please accept new baseline in " + message;
			// JOptionPane.showMessageDialog(null, message, "New Baseline", 1);
		} catch (TestFailedException f) {
			String message = f.getMessage().toString();
			message = message.substring(message.indexOf("See"));
			message = "verify element " + element + ": Failed!\n" + message;
			// JOptionPane.showMessageDialog(null, message, "Check Window", 1);
			Execution.setTestFail(message);
		}
	}

	public static void verifyRegion(String appName, String testName, WebElement element, String matchLevel) {
		try {
			// Execution.camera.apiKey = apiKey;
			// Execution.camera.screenSolution = screenSolution;
			// Execution.camera.matchLevel = matchLevel;
			Camera.captureElement(appName, "Element: " + testName, element, matchLevel);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		} catch (NewTestException b) {
			String message = b.getMessage().toString();
			message = message.substring(message.indexOf("https"));
			message = "Please accept new baseline in " + message;
			// JOptionPane.showMessageDialog(null, message, "New Baseline", 1);
		} catch (TestFailedException f) {
			String message = f.getMessage().toString();
			message = message.substring(message.indexOf("See"));
			message = "verify element " + element + ": Failed!\n" + message;
			// JOptionPane.showMessageDialog(null, message, "Check Window", 1);
			Execution.setTestFail(message);
		}
	}

	public static void verifyOrder(List<String> list, String order) {
		String prev = "";

		for (String current : list) {
			if (order.equalsIgnoreCase("asc")) {
				if (current.compareTo(prev) > 0) {
					Execution.setTestFail("The list is not sorted!");
				}
			} else {
				if (current.compareTo(prev) < 0) {
					Execution.setTestFail("The list is not sorted!");
				}
			}
		}
	}

	public static void verifyAttributeNotExist(WebElement element, String attribute) {
		String _temp = element.getAttribute(attribute);
		if (_temp != null) {
			Execution.setTestFail(attribute + " exists for " + element.toString());
		}
	}

	/**
	 * 
	 * @param Driver.instance
	 * @param by
	 */
	public static void verifyDisplayed(By by) {
		GeneralMethods.sleep(3000);
		WebElement element = GeneralMethods.getElement(by);
		if (!element.isDisplayed()) {
			Execution.setTestFail(element.toString() + " does not exists.");
		}
	}

	/**
	 * 
	 * @param Driver.instance
	 * @param element
	 */
	public static void verifyDisplayed(WebElement element) {
		GeneralMethods.sleep(3000);
		if (!element.isDisplayed()) {
			Execution.setTestFail(element.toString() + " is not displayed.");
		}
	}

	/**
	 * 
	 * @param by
	 */
	public static void verifyExists(By by) {
		WebElement element = GeneralMethods.getElement(by);
		if (element == null) {
			Execution.setTestFail(by.toString() + " does not exists.");
		}
	}

	/**
	 * 
	 * @param by
	 * @param seconds
	 */
	public static void verifyExists(By by, int seconds) {
		WebElement element = GeneralMethods.getElement(by, seconds);
		if (element == null) {
			Execution.setTestFail(by.toString() + " does not exists.");
		}
	}

	/**
	 * 
	 * @param element
	 */
	@SuppressWarnings("null")
	public static void verifyExists(WebElement element) {
		if (element == null) {
			Execution.setTestFail(element.toString());
		}
	}

	@SuppressWarnings("null")
	public static void verifyExists_v2(WebElement element, String data) {
		if (element == null) {
			Execution.setTestFail(element.toString() + " does not exists. With test data = " + data);
		}
	}

	public static void verifyExists_javascriptElement(String errorMessage, String data) {
		WebElement element = GeneralMethods
				.getElement(By.xpath(".//*[contains(text(),'" + errorMessage + "') and @style='display: block;']"));
		if (element == null) {
		}
		Execution.setTestFail(element + " does not exists. With test data = " + data);
	}

	public static void verifyNotExists_javascriptElement(String errorMessage, String data) {
		WebElement element = GeneralMethods
				.getElement(By.xpath(".//*[contains(text(),'" + errorMessage + "') and @style='display: block;']"));
		if (element != null) {
			Execution.setTestFail(element + " does not exists. With test data = " + data);
		}
	}

	/**
	 * 
	 * @param Driver.instance
	 * @param by
	 */
	public static void verifyNotDisplayed(By by) {
		GeneralMethods.sleep(3000);
		WebElement element = GeneralMethods.getElement(by);
		if (element.isDisplayed()) {
			Execution.setTestFail(element.toString() + " exists unexpectedly.");
		}
	}

	/**
	 * 
	 * @param Driver.instance
	 * @param element
	 */
	public static void verifyNotDisplayed(WebElement element) {
		GeneralMethods.sleep(3000);
		if (element.isDisplayed()) {
			Execution.setTestFail(element.toString() + " is displayed unexpectedly.");
		}
	}

	/**
	 * 
	 * @param Driver.instance
	 * @param by
	 */
	public static void verifyNotExist(By by) {
		Driver.setElementWait(10);
		WebElement element = GeneralMethods.getElement(by);
		if (element != null) {
			Execution.setTestFail(element.toString() + " exists unexpectedly.");
		}
		Driver.setElementWait(Execution.waitTime);
	}

	/**
	 * 
	 * @param Driver.instance
	 * @param element
	 */
	public static void verifyNotExist(WebElement element) {
		Driver.setElementWait(10);
		if (element != null) {
			Execution.setTestFail(element.toString() + " exists unexpectedly.");
		}
		Driver.setElementWait(Execution.waitTime);
	}

	public static void verifyNotExist_v2(WebElement element, String data) {
		Driver.setElementWait(10);
		if (element != null) {
			Execution.setTestFail(element + " exists unexpectedly. With test data = " + data);
		}
		Driver.setElementWait(Execution.waitTime);
	}

	/**
	 * 
	 * @param Driver.instance
	 * @param by
	 * @param property
	 * @param expected
	 */
	public static void verifyPropertyValue(By by, String property, String expected) {
		WebElement element = GeneralMethods.getElement(by);
		verifyPropertyValue(element, property, expected);
	}

	/**
	 * 
	 * @param Driver.instance
	 * @param element
	 * @param property
	 * @param expected
	 */
	public static void verifyPropertyValue(WebElement element, String property, String expected) {
		String actual;

		if (property.equalsIgnoreCase("checked")) {
			boolean status = element.isSelected();
			actual = Boolean.toString(status);
		} else {
			actual = element.getAttribute(property);
		}

		if (!actual.trim().equals(expected.trim())) {
			Execution.setTestFail(
					"Expected: " + property + "=" + expected + " || " + "Recorded: " + property + "=" + actual);
		}
	}

	/**
	 * 
	 * @param Driver.instance
	 * @param by
	 * @param property
	 * @param expected
	 */
	public static void verifyPropertyValueContains(By by, String property, String expected) {
		WebElement element = GeneralMethods.getElement(by);
		verifyPropertyValueContains(element, property, expected);
	}

	/**
	 * 
	 * @param Driver.instance
	 * @param element
	 * @param property
	 * @param expected
	 */
	@SuppressWarnings("null")
	public static void verifyPropertyValueContains(WebElement element, String property, String expected) {
		if (element != null) {
			String actual = element.getAttribute(property);
			System.out.println(actual);
			if (!actual.contains(expected) && !actual.matches(expected)) {
				Execution.setTestFail("verifyPropertyValueContains: " + property + " of " + element
						+ " does not contain " + expected);
			}
		} else
			Execution.setTestFail("verifyPropertyValueContains: " + element.toString() + " does not exists.");
	}

	public static void verifyPropertyValueNotContain(By by, String property, String expected) {
		WebElement element = GeneralMethods.getElement(by);
		verifyPropertyValueNotContain(element, property, expected);
	}

	/**
	 * 
	 * @param Driver.instance
	 * @param element
	 * @param property
	 * @param expected
	 */
	@SuppressWarnings("null")
	public static void verifyPropertyValueNotContain(WebElement element, String property, String expected) {
		if (element != null) {
			String actual = element.getAttribute(property);
			if (actual.contains(expected)) {
				Execution.setTestFail(
						"verifyPropertyValueNotContain: " + property + " of " + element + " contains " + expected);
			}
		} else
			Execution.setTestFail("verifyPropertyValueNotContain: " + element.toString() + " does not exists.");
	}

	/**
	 * 
	 * @param Driver.instance
	 * @param by
	 * @param text
	 */
	public static void verifyTextContains(By by, String text) {
		WebElement element = GeneralMethods.getElement(by);
		verifyTextContains(element, text);
	}

	/**
	 * 
	 * @param Driver.instance
	 * @param element
	 * @param text
	 */
	@SuppressWarnings("null")
	public static void verifyTextContains(WebElement element, String text) {
		if (element != null) {
			GeneralMethods.waitAttribute(element, "text", text, Execution.waitTime);
			String actual = element.getText();
			if (!actual.contains(text)) {
				Execution.setTestFail("verifyTextContains: " + actual + " does not contain " + text);
			}
		} else
			Execution.setTestFail("verifyTextContains: " + element.toString() + " does not exists.");
	}

	// Long update
	@SuppressWarnings("null")
	public static void verifyTextContains_v2(WebElement element, String text, String dataTest) {
		if (element != null) {
			GeneralMethods.waitAttribute(element, "text", text, Execution.waitTime);
			String actual = element.getText();
			if (!actual.contains(text)) {
				Execution.setTestFail(
						"verifyTextContains: " + actual + " does not contain " + text + " with test data " + dataTest);
			}
		} else
			Execution.setTestFail("verifyTextContains: " + element.toString() + " does not exists.");
	}

	/**
	 * 
	 * @param Driver.instance
	 * @param by
	 * @param text
	 */
	public static void verifyTextExists(By by, String text) {
		WebElement element = GeneralMethods.getElement(by);
		verifyTextExists(element, text);
	}

	/**
	 * 
	 * @param Driver.instance
	 * @param element
	 * @param text
	 */
	@SuppressWarnings("null")
	public static void verifyTextExists(WebElement element, String text) {
		if (element != null) {
			String actual = element.getText();
			if (!actual.trim().equals(text.trim())) {
				Execution.setTestFail("verifyTextExists: expected: " + text + " || " + "recorded: " + actual);
			}
		} else
			Execution.setTestFail("verifyTextExists: " + element.toString() + " does not exists.");
	}

	/**
	 * 
	 * @param Driver.instance
	 * @param by
	 * @param text
	 */
	public static void verifyTextExistsIgnoreCase(By by, String text) {
		WebElement element = GeneralMethods.getElement(by);
		verifyTextExistsIgnoreCase(element, text);
	}

	/**
	 * 
	 * @param Driver.instance
	 * @param element
	 * @param text
	 */
	@SuppressWarnings("null")
	public static void verifyTextExistsIgnoreCase(WebElement element, String text) {
		if (element != null) {
			String actual = element.getText();
			if (!actual.trim().equalsIgnoreCase(text.trim())) {
				Execution.setTestFail("verifyTextExistsIgnoreCase: expected: " + text + " || " + "recorded: " + actual);
			}
		} else
			Execution.setTestFail("verifyTextExistsIgnoreCase: " + element.toString() + " does not exists.");
	}

	/**
	 * 
	 * @param Driver.instance
	 * @param by
	 * @param text
	 */
	public static void verifyTextIn(By by, String text) {
		WebElement element = GeneralMethods.getElement(by);
		verifyTextIn(element, text);
	}

	/**
	 * 
	 * @param Driver.instance
	 * @param element
	 * @param text
	 */
	@SuppressWarnings("null")
	public static void verifyTextIn(WebElement element, String text) {
		if (element != null) {
			String actual = element.getText();
			if (actual.contains("..")) {
				actual = actual.split("\\..")[0];
			}
			if (!text.contains(actual)) {
				Execution.setTestFail("verifyTextIn: " + actual + " is not in " + text);
			}
		} else
			Execution.setTestFail("verifyTextIn: " + element.toString() + " does not exists.");
	}

	/**
	 * 
	 * @param Driver.instance
	 * @param by
	 * @param text
	 */
	public static void verifyTextNotContain(By by, String text) {
		WebElement element = GeneralMethods.getElement(by);
		verifyTextNotContain(element, text);
	}

	/**
	 * 
	 * @param Driver.instance
	 * @param element
	 * @param text
	 */
	@SuppressWarnings("null")
	public static void verifyTextNotContain(WebElement element, String text) {
		if (element != null) {
			String actual = element.getText();
			if (actual.contains(text)) {
				Execution.setTestFail("verifyTextNotContain: " + actual + " contains " + text + " unexpectedly.");
			}
		} else
			Execution.setTestFail("verifyTextNotContain: " + element.toString() + " does not exists.");
	}

	@SuppressWarnings("null")
	public static void verifyTextNotContain_v2(WebElement element, String text, String testDat) {
		if (element != null) {
			String actual = element.getText();
			if (actual.contains(text)) {
				Execution.setTestFail("verifyTextNotContain: " + actual + " contains " + text
						+ " unexpectedly. With test data: " + testDat);
			}
		} else
			Execution.setTestFail("verifyTextNotContain: " + element.toString() + " does not exists.");
	}

	/**
	 * 
	 * @param Driver.instance
	 * @param by
	 * @param text
	 */
	public static void verifyTextNotExist(By by, String text) {
		WebElement element = GeneralMethods.getElement(by);
		verifyTextNotExist(element, text);
	}

	/**
	 * 
	 * @param Driver.instance
	 * @param element
	 * @param text
	 */
	@SuppressWarnings("null")
	public static void verifyTextNotExist(WebElement element, String text) {
		if (element != null) {
			String actual = element.getText();
			if (actual.trim().equals(text.trim())) {
				Execution.setTestFail("verifyTextNotExist: " + text + " exists unexpectedly.");
			}
		} else
			Execution.setTestFail("verifyTextNotExist: " + element.toString() + " does not exists.");
	}

	/**
	 * @param Driver.instance
	 * @param by
	 * @param text
	 */
	public static void verifyValueContains(By by, String text) {
		WebElement element = GeneralMethods.getElement(by);
		verifyValueContains(element, text);
	}

	@SuppressWarnings("null")
	public static void verifyValueContains(WebElement element, String text) {
		if (element != null) {
			String actual = element.getAttribute("value");
			if (!actual.contains(text)) {
				Execution.setTestFail("verifyValueContains: " + actual + " does not contain " + text);
			}
		} else
			Execution.setTestFail("verifyValueContains: " + element.toString() + " does not exists.");
	}

	public static void verifyValueExists(WebDriver driver, By by, String text) {
		WebElement element = GeneralMethods.getElement(by);
		String actual = element.getAttribute("value");
		if (!actual.trim().equals(text.trim())) {
			Execution.setTestFail("Expected: " + text + " || " + "Recorded: " + actual);
		}
	}

	@SuppressWarnings("null")
	public static void verifyValueExists(WebElement element, String text) {
		if (element != null) {
			String actual = element.getAttribute("value");
			if (!actual.trim().equals(text.trim())) {
				Execution.setTestFail("Expected: " + text + " || " + "Recorded: " + actual);
			}
		} else
			Execution.setTestFail("verifyValueContains: " + element.toString() + " does not exists.");
	}

	public static void verifyValueExistsIgnoreCase(By by, String text) {
		WebElement element = GeneralMethods.getElement(by);
		String actual = element.getAttribute("value");
		if (!actual.trim().equalsIgnoreCase(text.trim())) {
			Execution.setTestFail("Expected: " + text + " || " + "Recorded: " + actual);
		}
	}

	public static void verifyValueExistsIgnoreCase(WebElement element, String text) {
		Driver.instance.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		String actual = element.getAttribute("value");
		if (!actual.trim().equalsIgnoreCase(text.trim())) {
			Execution.setTestFail("Expected: " + text + " || " + "Recorded: " + actual);
		}
	}

	public static void verifyValueNotExist(By by, String text) {
		WebElement element = GeneralMethods.getElement(by);
		String actual = element.getAttribute("value");
		if (actual.trim().equals(text.trim())) {
			Execution.setTestFail(text + " exists unexpectedly.");
		}
	}

	public static void verifyValueNotExist(WebElement element, String text) {
		String actual = element.getAttribute("value");
		if (actual.trim().equals(text.trim())) {
			Execution.setTestFail(text + " exists unexpectedly.");
		}
	}

	public static void verifyEnabled(WebElement element) {
		if (!element.isEnabled()) {
			Execution.setTestFail(element + " is not enabled.");
		}
	}

	public static void verifyAttributeExist(WebElement element, String attribute) {
		String _temp = element.getAttribute(attribute);
		if (_temp == null) {
			Execution.setTestFail(attribute + " does not exists for " + element.toString());
		}
	}

	/// <summary>
	/// Check if an item exists in a DropDown list
	/// </summary>
	/// <param name="element"></param>
	public static void verifyItemExists(WebElement element, String expectedItem) {
		if (expectedItem != null) {

			Execution.setTestFail("The option '" + expectedItem + "' does not exist in the list!");
		}
	}

	/// <summary>
	/// Check if an item exists in a DropDown list
	/// </summary>
	/// <param name="element"></param>
	public static void verifyItemExists(WebElement element, String elementName, String expectedItem) {
		if (expectedItem != null) {

			Execution.setTestFail("The option '" + expectedItem + "' of the element '" + elementName
					+ "' does not exist in the list!");
		}
	}

	/// <summary>
	/// Check if an element (Radio Button/Checkbox) is checked
	/// </summary>
	/// <param name="element"></param>
	///
	public static void verifyElementIsSelected(WebElement element) {
		GeneralMethods.waitForPageFullyLoaded(Execution.pageWait);
		if (element.isSelected() != true) {
			Execution.setTestFail("The element '" + element.toString() + "' is not selected!");
		}
	}

	/// <summary>
	/// Check if an element (Radio Button/Checkbox) is checked
	/// </summary>
	/// <param name="element"></param>
	///
	public static void verifyElementIsSelected(WebElement element, String elementName) {
		GeneralMethods.waitForPageFullyLoaded(Execution.pageWait);
		if (element.isSelected() != true) {
			Execution.setTestFail("The element '" + elementName + "' is not selected!");
		}
	}

	/// <summary>
	/// Check if an element (Radio Button/Checkbox) is not checked
	/// </summary>
	/// <param name="element"></param>
	public static void verifyElementIsNotSelected(WebElement element) {
		GeneralMethods.waitForPageFullyLoaded(Execution.pageWait);
		if (element.isSelected() == true) {
			Execution.setTestFail("The element '" + element.toString() + "' is selected!");
		}
	}

	/// <summary>
	/// Check if an element (Radio Button/Checkbox) is not checked
	/// </summary>
	/// <param name="element"></param>
	public static void verifyElementIsNotSelected(WebElement element, String elementName) {
		GeneralMethods.waitForPageFullyLoaded(Execution.pageWait);
		if (element.isSelected() == true) {
			Execution.setTestFail("The element '" + elementName + "' is selected!");
		}
	}

	public static void verifyColor(WebElement element, String hexColor) {
		String color = element.getCssValue("color");
		String s1 = color.substring(5);
		StringTokenizer st = new StringTokenizer(s1);
		int r = Integer.parseInt(st.nextToken(",").trim());
		int g = Integer.parseInt(st.nextToken(",").trim());
		int b = Integer.parseInt(st.nextToken(",").trim());
		Color c = new Color(r, g, b);
		String hex = "#" + Integer.toHexString(c.getRGB()).substring(2);
		VerifyMethods.verifyTextEquals(hex, hexColor);
	}

	public static void verifyEnabled(By by) {
		WebElement element = GeneralMethods.getElement(by);
		if (!element.isEnabled()) {
			Execution.setTestFail(element + " is not enabled.");
		}
	}

	public static void verifyDisabled(WebElement element) {
		if (element.isEnabled()) {
			Execution.setTestFail(element + " is not disabled.");
		}
	}

	public static void verifyDisabled(By by) {
		WebElement element = GeneralMethods.getElement(by);
		if (element.isEnabled()) {
			Execution.setTestFail(element + " is not disabled.");
		}
	}

	public static void verifySelectedItem(WebElement element, String item) {
		org.openqa.selenium.support.ui.Select cboElement = new org.openqa.selenium.support.ui.Select(element);
		VerifyMethods.verifyTextEquals(cboElement.getFirstSelectedOption().getText(), item);
	}

	public static void verifySelectedItem(By by, String item) {
		org.openqa.selenium.support.ui.Select cboElement = new org.openqa.selenium.support.ui.Select(
				GeneralMethods.getElement(by));
		VerifyMethods.verifyTextEquals(cboElement.getFirstSelectedOption().getText(), item);
	}

	public static void verifyVisible(By by) {
		WebElement element = GeneralMethods.getElement(by);
		if (element.isEnabled()) {
			Execution.setTestFail(element + " is not disabled.");
		}
	}

	public static void verifyTooltipContains(By by, String text) {
		WebElement element = GeneralMethods.getElement(by);
		String recordedText = element.getAttribute("data-original-title");

		if (recordedText == null) {
			recordedText = element.getAttribute("title");
		}

		String tmp = recordedText.replace("\n", " ");
		tmp = tmp.replace("</p><p class='nowrap'>", " ");
		tmp = tmp.replace("<p class='nowrap'>", " ");
		tmp = tmp.replace("</p>", " ");
		tmp = tmp.replace("<br>", " ");
		recordedText = tmp;

		if (!recordedText.contains(text)) {
			Execution.setTestFail(recordedText + " does not contain " + text);
		}
	}

	public static void verifyTooltipNotContain(By by, String text) {
		WebElement element = GeneralMethods.getElement(by);
		String recordedText = element.getAttribute("data-original-title");

		if (recordedText == null) {
			recordedText = element.getAttribute("title");
		}

		String tmp = recordedText.replace("\n", " ");
		tmp = tmp.replace("</p><p class='nowrap'>", " ");
		tmp = tmp.replace("<p class='nowrap'>", " ");
		tmp = tmp.replace("</p>", " ");
		tmp = tmp.replace("<br>", " ");
		recordedText = tmp;

		if (recordedText.contains(text)) {
			Execution.setTestFail(recordedText + " contains " + text);
		}
	}

	public static void verifyTooltipContains(WebElement element, String text) {
		String recordedText = element.getAttribute("data-original-title");

		if (recordedText == null) {
			recordedText = element.getAttribute("title");
		}

		String tmp = recordedText.replace("\n", " ");
		tmp = tmp.replace("</p><p class='nowrap'>", " ");
		tmp = tmp.replace("<p class='nowrap'>", " ");
		tmp = tmp.replace("</p>", " ");
		tmp = tmp.replace("<br>", " ");
		recordedText = tmp;

		if (!text.equals("")) {
			if (!recordedText.contains(text)) {
				Execution.setTestFail(recordedText + " does not contain " + text);
			}
		} else {
			if (!recordedText.equals("")) {
				Execution.setTestFail("Expected: <no tooltip> | Recorded: " + recordedText);
			}
		}
	}

	public static void verifyPageExist(String expected) {
		WebDriver window = null;
		String existed = "no";
		Iterator<String> windowIterator = Driver.instance.getWindowHandles().iterator();
		while (windowIterator.hasNext()) {
			String windowHandle = windowIterator.next();
			// System.out.println(windowHandle);
			window = Driver.instance.switchTo().window(windowHandle);
			// System.out.println(window.getTitle());
			if (window.getTitle().contains(expected)) {
				existed = "yes";
				break;
			}
		}

		if (existed.equalsIgnoreCase("no"))
			Execution.setTestFail("Page does not exist");
	}

	public static void verifyTooltipNotContain(WebElement element, String text) {
		String recordedText = element.getAttribute("data-original-title");

		if (recordedText == null) {
			recordedText = element.getAttribute("title");
		}

		String tmp = recordedText.replace("\n", " ");
		tmp = tmp.replace("</p><p class='nowrap'>", " ");
		tmp = tmp.replace("<p class='nowrap'>", " ");
		tmp = tmp.replace("</p>", " ");
		tmp = tmp.replace("<br>", " ");
		recordedText = tmp;

		if (recordedText.contains(text)) {
			Execution.setTestFail(recordedText + " contains " + text);
		}
	}

}
