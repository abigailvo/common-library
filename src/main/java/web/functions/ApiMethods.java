package web.functions;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.Hashtable;
import java.util.Map.Entry;
import java.util.Set;
import java.util.zip.GZIPInputStream;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreConnectionPNames;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ApiMethods {

	public static String getResponseFromGET(String URL) {
		StringBuilder sb = new StringBuilder();
		URLConnection urlConn = null;
		InputStreamReader in = null;
		try {

			URL url = new URL(URL);
			urlConn = url.openConnection();
			urlConn.addRequestProperty("User-Agent",
					"Mozilla/5.0 (Windows NT 6.2; WOW64; Trident/7.0; rv:11.0) like Gecko");

			if (urlConn != null) {
				urlConn.setReadTimeout(60 * 1000);
			}
			if (urlConn != null && urlConn.getInputStream() != null) {
				in = new InputStreamReader(urlConn.getInputStream(), Charset.defaultCharset());
				BufferedReader bufferedReader = new BufferedReader(in);
				if (bufferedReader != null) {
					int cp;
					while ((cp = bufferedReader.read()) != -1) {
						sb.append((char) cp);
					}
					bufferedReader.close();
				}
			}
			in.close();
		} catch (Exception e) {
			throw new RuntimeException("Exception while calling URL: " + URL, e);
		}
		return sb.toString();
	}

	/**
	 * @author TramVo
	 * @param URL
	 * @param authorization
	 * @return
	 */
	public static String getResponseFromGET(String URL, String authorization) {
		String url = URL;
		StringBuffer result = new StringBuffer();
		try {
			HttpClient client = new DefaultHttpClient();
			HttpGet get = new HttpGet(url);
			get.setHeader("Accept-Encoding", "gzip, deflate");
			get.setHeader("Content-Type", "application/json");
			// get.setHeader("authorization", authorization);
			get.setHeader(".ASPXAUTH", authorization);

			int timeout = 60;
			org.apache.http.params.HttpParams httpParams = client.getParams();
			httpParams.setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, timeout * 1000);
			httpParams.setParameter(CoreConnectionPNames.SO_TIMEOUT, timeout * 1000);

			HttpResponse response = client.execute(get);

			BufferedReader rd;
			try {
				rd = new BufferedReader(new InputStreamReader(new GZIPInputStream(response.getEntity().getContent())));
			} catch (Exception e) {
				rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			}

			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
		} catch (Exception e) {
		}
		return result.toString();
	}

	/**
	 * 
	 */
	public static HttpResponse getResponseFromPOST(String APIurl, String parameter) {
		String url = APIurl;
		StringBuffer result = new StringBuffer();
		HttpResponse response = null;
		try {
			HttpClient client = new DefaultHttpClient();
			HttpPost post = new HttpPost(url);
			// post.setHeader("Accept-Encoding", "gzip, deflate, br");
			post.setHeader("Content-Type", "application/json");

			int timeout = 60;
			org.apache.http.params.HttpParams httpParams = client.getParams();
			httpParams.setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, timeout * 1000);
			httpParams.setParameter(CoreConnectionPNames.SO_TIMEOUT, timeout * 1000);

			StringEntity input = new StringEntity(parameter);
			input.setContentType("application/json");
			post.setEntity(input);

			response = client.execute(post);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	public static String extractJSONFromResponse(HttpResponse response) {
		StringBuffer json = new StringBuffer();
		try {
			// BufferedReader rd = new BufferedReader(
			// new InputStreamReader(new
			// GZIPInputStream(response.getEntity().getContent())));
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

			String line = "";
			while ((line = rd.readLine()) != null) {
				json.append(line);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return json.toString();
	}

	/**
	 * 
	 * @param apiUrl
	 *            api url
	 * @param parameter
	 *            parameter string (ex:
	 *            {\"country\":\"VietNam\",\"zipcode\":8000})
	 * @param authorization
	 *            login token string
	 * @return response message
	 */
	public static HttpResponse getResponseFromPOST(String apiUrl, String parameter, String authorization) {
		String url = apiUrl;
		StringBuffer result = new StringBuffer();
		HttpResponse response = null;
		try {
			HttpClient client = new DefaultHttpClient();
			HttpPost post = new HttpPost(url);
			post.setHeader("Content-Type", "application/json");
			post.setHeader(".ASPXAUTH", authorization);

			int timeout = 60;
			org.apache.http.params.HttpParams httpParams = client.getParams();
			httpParams.setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, timeout * 1000);
			httpParams.setParameter(CoreConnectionPNames.SO_TIMEOUT, timeout * 1000);

			StringEntity input = new StringEntity(parameter);
			input.setContentType("application/json");
			post.setEntity(input);

			response = client.execute(post);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	public static HttpResponse getResponseFromGETWithCookies(String URL, String cookies) {
		String url = URL;
		StringBuffer result = new StringBuffer();
		int i = 0;
		HttpResponse response = null;
		try {
			do {
				HttpClient client = new DefaultHttpClient();
				HttpPost post = new HttpPost(url);
				post.setHeader("Accept-Encoding", "gzip, deflate, br");
				post.setHeader("Cookie", cookies);
				post.setHeader("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:49.0) Gecko/20100101 Firefox/49.0");
				response = client.execute(post);
				i++;
			} while (i <= 3);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}
}