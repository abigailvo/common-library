package web.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import web.functions.GeneralMethods;



public class PgCommon {
	
	public static WebElement txtUsername(){
		return GeneralMethods.getElement(By.xpath(".//input[@id='InputEmail']"));
	}
	
	public static WebElement txtPassword(){
		return GeneralMethods.getElement(By.xpath(".//input[@id='InputPassword']"));
	}
	
	public static WebElement btLogin(){
		return GeneralMethods.getElement(By.xpath("//button[@type='submit']"));
	}
	
	////i[@class='fa fa-bars']
	
	public static WebElement menuAfterLogin(){
		return GeneralMethods.getElement(By.xpath("//i[@class='fa fa-bars']"));
	}
	
	public static WebElement btLogout(){
		return GeneralMethods.getElement(By.xpath("//span[contains(text(),'log out')]"));
	}
	
	public static WebElement lnkForgotPass(){
		return GeneralMethods.getElement(By.xpath("//a[text()='forgot password?']"));
	}
	
	public static WebElement txtEmailAccount(){
		return GeneralMethods.getElement(By.xpath(".//*[@id='InputEmail']"));
	}
	public static WebElement btResetPass(){
		return GeneralMethods.getElement(By.xpath("//button[contains(text(),'reset')]")); 
	}
	
	//Reset password page
	public static WebElement txtNewPassword(){
		return  GeneralMethods.getElement(By.xpath(".//*[@id='InputNewPassword']"));
	}
	public static WebElement txtConfirmNewPassword(){
		return  GeneralMethods.getElement(By.xpath(".//*[@id='InputConfirmPassword']"));
	}
	public static WebElement btUpdatePass(){
		return GeneralMethods.getElement(By.xpath("//button[text()='update my password']"));
	}
	
	
	public static WebElement errorElement(){
		return GeneralMethods.getElement(By.xpath(".//*[@id='ErrorMessage']"));
	}
	
	// some information of email system
	public static String subjectEmail = "Reset your givn password";
	public static String fromEmail="support-staging@givn.social";
	
	
}
