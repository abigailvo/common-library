package common;

import web.functions.GeneralMethods;

public class Constants {

	public static class Framework {
		public static String DRIVER_FOLDER = System.getProperty("user.home") + "//SeleniumDrivers//";
		public static String CHROME_DRIVER_PATH = DRIVER_FOLDER + "chromedriver";
		public static String FF_DRIVER_PATH = DRIVER_FOLDER + "FirefoxDriver.exe";
		public static String IE_DRIVER_PATH = DRIVER_FOLDER + "IEDriverServer.exe";
		public static String SAFARI_DRIVER_PATH = DRIVER_FOLDER + "SafariDriver.exe";
	}

}
