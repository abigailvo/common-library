package common;
import java.awt.datatransfer.ClipboardOwner;
import java.io.ByteArrayOutputStream;
import java.lang.reflect.Method;
import java.util.List;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import web.Driver;
import web.functions.GeneralMethods;
import web.functions.Camera;

public class Execution {
	public static int pageWait;
	public static int implicitWait;
	public static int waitTime;
	public static Method method;
	public static String configuration;
	public static String className;
	public static Camera camera;
	public static String apiKey;
	public static String screenSolution;
	public static String matchLevel = "";
	// Variables for tracking test results
	public static boolean isPassed;
	public static  int length = 0;
	public static  int count = 0;
	public static  List<Boolean> arrayTestResult;
	public static  String result = "";
	public static String[] keys;
	public static String[] components;
	public static int projectId;
	public static String planName;
	public static String clientId;
	public static String testrailUsername;
	public static String testrailPassword;
	public static ClipboardOwner clipboardOwner;
    public static String executionMode;
    public static WebDriverWait webDriverWait;

	public static  final String FILES_PATH = GeneralMethods.getFilesPath("src//main//java//web//dataset//");
	public static  final String CONFIGURATION_FILE_PATH = GeneralMethods.getFilesPath("src//main//java//initiations//");
	
	public static ByteArrayOutputStream outputStream = null;

	public static void setTestFail(String log) {
		Reporter.log(log);
		// Reporter.log("[" + captureScreenshot() + "]");
		if (method != null) {
			System.out.println(
					"[FAILED] " + method.getDeclaringClass().getName() + "." + method.getName() + " >>> " + log);
		} else
			System.out.println("[FAILED] >>> " + log);
		isPassed = false;
	}

	public static void runBeforeMethod(Method method) {
		System.out.println("----------------------\n******** STARTED RUNNING: " + method.getName()
				+ " ********\n----------------------");
		isPassed = true;
	}
	
	public void setTestFail(Method method, String log) {
		Reporter.log(log);
		// Reporter.log("[" + captureScreenshot() + "]");
		if (method != null) {
			System.out.println(
					"[FAILED] " + method.getDeclaringClass().getName() + "." + method.getName() + " >>> " + log);
		} else
			System.out.println("[FAILED] >>> " + log);
		isPassed = false;
	}

	public static void initialize()
    {
		webDriverWait = new WebDriverWait(Driver.instance, waitTime); 
		// Wait for element
		Driver.setElementWait(implicitWait);
		
		// Wait for page load
		Driver.setPageWait(pageWait);
        Driver.instance.manage().window().maximize();
       
    }
}
