package common.thirdParty;

import java.lang.reflect.Method;

import com.rmn.testrail.entity.TestInstance;
import com.rmn.testrail.entity.TestResult;
import com.rmn.testrail.entity.TestRun;
import com.rmn.testrail.entity.TestRunGroup;
import com.rmn.testrail.service.TestRailService;

import web.functions.GeneralMethods;

public class TestRail {
	public TestResult result;
	public String testresultName;
	public TestRailService trs;
	public int statusId;
	public String comment;
	public Method method;
	public int projectId;
	public String planName;
	public String configuration;
	public String component = "";
	public String[] arrKey;
	public String[] arrComponent;
	private String className;
	public String clientId;
	public String username;
	public String password;

	public TestRail(Method method, String configuration, String currentClassName) {
		statusId = 3;
		result = new TestResult();
		this.method = method;
		this.configuration = configuration;
		className = currentClassName;
	}

	public int getPlanId() {
		trs = new TestRailService(clientId, username, password);
		int i = 0;
		int planId = -1;
		while (i < 5) {
			try {
				if (trs.getProject(projectId).getTestPlanByName(planName) != null) {
					planId = trs.getProject(projectId).getTestPlanByName(planName).getId();
					break;
				} else
					i++;
			} catch (Exception e) {
				System.out.println("Could not find test plan: " + planName);
				i++;
			}
			GeneralMethods.sleep(5000);
		}
		return planId;
	}

	public void addResult() {
		int stop = 0;
		int i = 0;

		// Get component
		String id1 = className.substring(0, 2);
		String id2 = className.substring(0, 3);
		for (int n = 0; n < arrKey.length; n++) {
			if (arrKey[n].equals(id1) | arrKey[n].equals(id2))
				component = arrComponent[n];
			System.out.println("COMPONENT: " + component);
		}

		if (configuration.toLowerCase().equals("firefox"))
			configuration = "ff";
		int planId = getPlanId();
		if (planId > -1) {
			while (i < 5) {
				try {
					for (TestRunGroup trg : trs.getTestPlan(planId).getEntries()) {
						for (TestRun testrun : trg.getRuns()) {
							if (testrun.getName().contains(component)) {
								if (testrun.getConfig() != null) {
									if (testrun.getConfig().toLowerCase().contains(configuration.toLowerCase())) {
										testresultName = this.method.getName().replaceAll("_", " ");
										for (TestInstance test : trs.getTests(testrun.getId())) {
											if (test.getTitle().toLowerCase().equals(testresultName.toLowerCase())) {
												result.setStatusId(statusId);
												result.setAssignedtoId(1);
												result.setComment(comment);
												trs.addTestResult(test.getId(), result);
												stop = 1;
											}
											if (stop == 1)
												break;
										}
									}
								}
							}
							if (stop == 1)
								break;
						}
						if (stop == 1)
							break;
					}
					i = 5;
				} catch (RuntimeException e) {
					i = i + 1;
					if (i == 5) {
						System.out.println("Unable to connect to TestRail server");
					} else
						GeneralMethods.sleep(5000);
				}
			}
		}
	}

}